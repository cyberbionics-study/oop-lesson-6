"""
Пример реализации списка с итератором
"""


class MyList(object):
    """Класс списка"""

    class _ListNode(object):
        """Внутренний класс элемента списка"""

        __slots__ = ('value', 'prev', 'next')

        def __init__(self, value, prev=None, next=None):
            self.value = value
            self.prev = prev
            self.next = next

        def __repr__(self):
            return 'MyList._ListNode({}, {}, {})'.format(self.value, id(self.prev), id(self.next))

    class _Iterator(object):
        """Внутренний класс итератора"""

        def __init__(self, list_instance):
            self._list_instance = list_instance
            self._next_node = list_instance._head

        def __iter__(self):
            return self

        def __next__(self):
            if self._next_node is None:
                raise StopIteration

            value = self._next_node.value
            self._next_node = self._next_node.next

            return value

    def __init__(self, iterable=None):
        self._length = 0
        self._head = None
        self._tail = None

        if iterable is not None:
            for element in iterable:
                self.append(element)

    def append(self, element):
        """Добавление элемента в конец списка"""

        node = MyList._ListNode(element)

        if self._tail is None:
            self._head = self._tail = node
        else:
            self._tail.next = node
            node.prev = self._tail
            self._tail = node

        self._length += 1

    def clear(self):
        """Очищение списка от содержимого."""
        current_node = self._head
        while current_node is not None:
            next_node = current_node.next
            current_node.prev = None
            current_node.next = None
            current_node = next_node

        self._head = self._tail = None
        self._length = 0

    def append_in_index(self, element, index):

        if index < 0 or index > len(self):
            raise IndexError('list index out of range')

        new_node = MyList._ListNode(element)

        if index == 0:
            new_node.next = self._head
            self._head.prev = new_node
            self._head = new_node

        elif index == len(self):
            new_node.prev = self._tail
            self._tail.next = new_node
            self._tail = new_node
        else:
            current_node = self._head
            for i in range(index):
                current_node = current_node.next

            prev_node = current_node.prev
            prev_node.next = new_node
            new_node.prev = prev_node
            new_node.next = current_node
            current_node.prev = new_node

        self._length += 1

    def delete_element(self, index=None):
        pass

    def __len__(self):
        return self._length

    def __repr__(self):
        return 'MyList([{}])'.format(', '.join(map(repr, self)))

    def __getitem__(self, index):
        if not 0 <= index < len(self):
            raise IndexError('list index out of range')

        node = self._head
        for _ in range(index):
            node = node.next

        return node.value

    def __iter__(self):
        return MyList._Iterator(self)


def main():
    my_list = MyList([1, 2, 3, 4, 5, 6, 7, 8, 9])

    print(len(my_list))

    print(my_list)

    print()

    for element in my_list:
        print(element)

    print()

    for element in my_list:
        print(element)

    my_list.append_in_index(12, 1)

    print(my_list)

    my_list.clear()

    print(my_list)

    new_list = MyList()

    new_list.append_in_index(12, 2)

    print(new_list)


if __name__ == '__main__':
    main()
