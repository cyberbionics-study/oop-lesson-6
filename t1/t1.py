iterable = []

for i in range(10):
    iterable.append(i)

for step in iterable:
    print(f'{step} was added on {step+1} step of previous "for" cycle')
