
class ReverseIterator:
    def __init__(self, lst: list):
        self.lst = lst
        self.index = len(lst) - 1

    def __iter__(self):
        return self

    def __next__(self):
        if self.index >= 0:
            result = self.lst[self.index]
            self.index -= 1
            return result
        else:
            raise StopIteration


my_list = [1, 2, 3, 4, 5]
reverse_iter = ReverseIterator(my_list)

for item in reverse_iter:
    print(item)